# CARIMAM_DOCC10

Convolutionnal neural networks for click classification.

Call the python script followed by the list of wav files you would like to process, and optionnaly an output filename.
For example : 
`python forward_UpDimV2_long.py path/to/file_or_folder --output_path predictions.pkl`

Use `python forward_UpDimV2_long.py -h` to display the help message with all the possible argmuments and their usage.

The required libraries are PIL, torch, torchvision, torchelie, numpy, soundfile, scipy and tqdm
